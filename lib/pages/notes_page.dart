import 'package:flutter/material.dart';
import 'package:flutter_sqllite/db/notes_database.dart';
import 'package:flutter_sqllite/models/note.dart';
import 'package:flutter_sqllite/pages/add_edit_note_page.dart';
import 'package:flutter_sqllite/pages/note_detail_page.dart';
import 'package:flutter_sqllite/widgets/note_card_widget.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class NotesPage extends StatefulWidget {
  const NotesPage({Key? key}) : super(key: key);

  @override
  State<NotesPage> createState() => _NotesPageState();
}

class _NotesPageState extends State<NotesPage> {
  late List<Note> notes;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    refreshNotes();
  }

  @override
  void dispose() {
    super.dispose();
    NotesDatabase.instance.close();
  }

  Future refreshNotes() async {
    setState(() => isLoading = true);
    notes = await NotesDatabase.instance.readAllNotes();
    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notes Page'),
      ),
      body: Center(
        child: (isLoading == false
            ? (notes.isEmpty ? const Text('No Notes') : _buildNotes())
            : const CircularProgressIndicator(
                color: Colors.green,
              )),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const AddEditNotePage(),
              ));
          refreshNotes();
        },
        backgroundColor: const Color.fromARGB(255, 125, 92, 81),
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget _buildNotes() => StaggeredGridView.countBuilder(
        itemCount: notes.length,
        crossAxisCount: 4,
        mainAxisSpacing: 4,
        crossAxisSpacing: 4,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () async {
            await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      NoteDetailPage(noteId: notes[index].id!),
                ));
            refreshNotes();
          },
          child: NoteCardWidget(note: notes[index], index: index),
        ),
        staggeredTileBuilder: (index) => const StaggeredTile.fit(2),
      );
}
